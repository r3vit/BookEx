<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
	<div class="box-body">
	  <!-- /.box-header -->
	  <form>
	    <table id="example2" class="table table-bordered table-hover">
	      <thead>
	        <tr>
	          <th>ISBN</th>
	          <th>Proprietario</th>
	          <th>Titolo</th>
	          <th>Autore</th>
	          <th colspan=3 class="text-center">Action</th>
	        </tr>
	      </thead>
	      <tbody>
	        <c:forEach var="book" items="${bookList}">
	          <tr>
	          	<td>${book.isbn}</td>
	            <td>${book.proprietario}</td>
	            <td>${book.titolo}</td>
	            <td>${book.autore}</td>
	            <td>
	            <td>
		            <c:choose>
		            	<c:when test = "${book.stato == 'disponibile'}">
		            		<a href="../reserve/${book.isbn}">Prenota</a>
		            	</c:when>
		            	<c:otherwise>
		            		non disponibile
		            	</c:otherwise>
		            </c:choose>
				</td>
	          </tr>
	        </c:forEach>
	      </tbody>
	    </table>
	  </form>
	</div>
</body>
</html>
      
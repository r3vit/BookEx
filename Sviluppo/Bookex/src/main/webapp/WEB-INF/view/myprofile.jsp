<%@taglib prefix="sec"
   uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>BookEx</title>
      <base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href="favicon.ico">
      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="assets/bower_components/font-awesome/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="assets/bower_components/Ionicons/css/ionicons.min.css">
      <!-- Select2 -->
      <link rel="stylesheet" href="assets/bower_components/select2/dist/css/select2.min.css">
      <!-- jvectormap -->
      <link rel="stylesheet" href="assets/bower_components/jvectormap/jquery-jvectormap.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
      <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
      <link rel="stylesheet" href="assets/dist/css/skins/_all-skins.min.css">
   </head>
   <body>
      <body class="hold-transition skin-blue fixed sidebar-mini">
         <div class="wrapper">
            <!-- Main Header -->
            <header class="main-header">
               <!-- Logo -->
               <a href="home" class="logo">
                  <!-- mini logo for sidebar mini 50x50 pixels -->
                  <span class="logo-mini"><b>BE</b>x</span>
                  <!-- logo for regular state and mobile devices -->
                  <span class="logo-lg"><b>BookEx</b></span>
               </a>
               <!-- Header Navbar -->
               <nav class="navbar navbar-static-top" role="navigation">
                  <!-- Sidebar toggle button-->
                  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                  <span class="sr-only">Toggle navigation</span>
                  </a>
                  <div class="navbar-custom-menu">
                     <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                           <img src="img/${userData.username}.jpg" class="user-image" alt="User Image">
                           <span class="hidden-xs">${userData.username} </span>
                           </a>
                           <ul class="dropdown-menu">
                              <!-- User image -->
                              <li class="user-header">
                                 <img src="img/${userData.username}.jpg" class="img-circle" alt="User Image">
                                 <p>
                                    ${userData.nome} ${userData.cognome}
                                    <small>punti disponibili: ${userData.punti}</small>
                                    <small>punti impegnati: ${userData.puntiImpegnati}</small>
                                 </p>
                              </li>
                              <!-- Menu Footer-->
                              <li class="user-footer">
                                 <div class="pull-left">
                                    <a href="myprofile" class="btn btn-default btn-flat">Profilo</a>
                                 </div>
                                 <div class="pull-right">
                                    <a href="javascript:formSubmit()" class="btn btn-danger btn-flat" >Esci</a>
                                    <c:url value="/logout" var="logoutUrl" />
                                    <form action="${logoutUrl}" method="post" id="logoutForm">
                                       <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    </form>
                                 </div>
                              </li>
                           </ul>
                        </li>
                     </ul>
                  </div>
               </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
               <!-- sidebar: style can be found in sidebar.less -->
               <section class="sidebar">
                  <!-- Sidebar Menu -->
                  <ul class="sidebar-menu" data-widget="tree">
                     <li class="header">BookEx</li>
                     <!-- Optionally, you can add icons to the links -->
                     <li class="active"><a href="myprofile"><i class="fa fa-user"></i> <span>Il mio profilo</span></a></li>
                     <li class="active"><a href="mybooks"><i class="fa fa-book"></i> <span>I miei libri</span></a></li>
                     <li class="active"><a href="addbook"><i class="fa fa-plus"></i> <span>Aggiungi libro</span></a></li>
                     <li class="active"><a href="mytransactions"><i class="fa fa-exchange"></i> <span>Le mie prenotazioni</span></a></li>
                     <li class="active"><a href="search"><i class="fa fa-search"></i> <span>Ricerca un libro</span></a></li>
                  </ul>
                  <!-- /.sidebar-menu -->
               </section>
               <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
               <!-- Main content -->
               <section class="content">
                  <div class="row">
                     <div class="col-md-3">
                        <!-- Profile Image -->
                        <div class="box box-primary">
                           <div class="box-header with-border">
                              <h3 class="box-title">Tu</h3>
                           </div>
                           <div class="box-body box-profile">
                              <img class="profile-user-img img-responsive img-circle" src="img/${userData.username}.jpg" alt="User profile picture">
                              <h3 class="profile-username text-center">${userData.username}</h3>
                              <ul class="list-group list-group-unbordered">
                                 <li class="list-group-item">
                                    <b>Nome</b> <a class="pull-right">${userData.nome}</a>
                                 </li>
                                 <li class="list-group-item">
                                    <b>Cognome</b> <a class="pull-right">${userData.cognome}</a>
                                 </li>
                                 <li class="list-group-item">
                                    <b>Sesso</b> <a class="pull-right">${userData.sesso}</a>
                                 </li>
                                 <li class="list-group-item">
                                    <b>Punti</b> <a class="pull-right">${userData.punti}</a>
                                 </li>
                                 <li class="list-group-item">
                                    <b>Punti impegnati</b> <a class="pull-right">${userData.puntiImpegnati}</a>
                                 </li>
                              </ul>
                           </div>
                           <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        <!-- About Me Box -->
                        <div class="box box-primary">
                           <div class="box-header with-border">
                              <h3 class="box-title">Recapiti</h3>
                           </div>
                           <!-- /.box-header -->
                           <div class="box-body">
                              <strong><i class="fa fa-map-marker margin-r-5"></i> Indirizzo di consegna</strong>
                              <p class="text-muted"> </p>
                              <strong>Indirizzo: </strong> 
                              <p><a>${userData.indirizzo}</a></p>
                              <strong>Citt�: </strong> 
                              <p><a>${userData.citta}</a></p>
                              <strong>Provincia: </strong> 
                              <p><a>${userData.provincia}</a></p>
                              <strong>CAP: </strong> 
                              <p><a>${userData.cap}</a></p>
                              <hr>
                              <strong><i class="fa fa-phone margin-r-5"></i> Telefono</strong>
                              <p class="text-muted"><a>${userData.telefono}</a></p>
                              <hr>
                              <strong><i class="fa fa-envelope margin-r-5"></i> email</strong>
                              <p class="text-muted"><a>${userData.email}</a></p>
                              <hr>
                           </div>
                           <!-- /.box-body -->
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- /. Content Wrapper. Contains page content -->
            <!-- Main Footer -->
            <footer class="main-footer">
               <strong> BookEx </strong> source code is available on <a href="https://gitlab.com/r3vit/BookEx/" target="_blank">GitLab</a> <i class="fa fa-gitlab"></i>
            </footer>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
               <!-- Create the tabs -->
               <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                  <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                  <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
               </ul>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
               immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
         </div>
         <!-- jQuery 3 -->
         <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
         <!-- Bootstrap 3.3.7 -->
         <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
         <!-- FastClick -->
         <script src="assets/bower_components/fastclick/lib/fastclick.js"></script>
         <!-- AdminLTE App -->
         <script src="assets/dist/js/adminlte.min.js"></script>
         <!-- Sparkline -->
         <script src="assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
         <!-- jvectormap  -->
         <script src="assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
         <script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
         <!-- SlimScroll -->
         <script src="assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
         <!-- ChartJS -->
         <script src="assets/bower_components/chart.js/Chart.js"></script>
         <!-- Select2 -->
         <script src="assets/bower_components/select2/dist/js/select2.full.min.js"></script>
         <!-- AdminLTE for demo purposes -->
         <script src="assets/dist/js/demo.js"></script>
         <script>
            $(function () {
              //Initialize Select2 Elements
              $('.select2').select2()
            
              //Datemask dd/mm/yyyy
              $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
              //Datemask2 mm/dd/yyyy
              $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
              //Money Euro
              $('[data-mask]').inputmask()
            
              //Date range picker
              $('#reservation').daterangepicker()
              //Date range picker with time picker
              $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
              //Date range as a button
              $('#daterange-btn').daterangepicker(
                {
                  ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                  },
                  startDate: moment().subtract(29, 'days'),
                  endDate  : moment()
                },
                function (start, end) {
                  $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
              )
            
              //Date picker
              $('#datepicker').datepicker({
                autoclose: true
              })
            
              //iCheck for checkbox and radio inputs
              $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
              })
              //Red color scheme for iCheck
              $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
              })
              //Flat red color scheme for iCheck
              $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
              })
            
              //Colorpicker
              $('.my-colorpicker1').colorpicker()
              //color picker with addon
              $('.my-colorpicker2').colorpicker()
            
              //Timepicker
              $('.timepicker').timepicker({
                showInputs: false
              })
            })
         </script>
         <script>
            function formSubmit() {
            	document.getElementById("logoutForm").submit();
            }
         </script>
   </body>
</html>

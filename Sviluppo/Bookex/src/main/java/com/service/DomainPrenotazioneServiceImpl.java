package com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Copia;
import com.model.DomainLibro;
import com.model.DomainPrenotazione;
import com.model.Libro;
import com.model.Prenotazione;

@Service
public class DomainPrenotazioneServiceImpl implements DomainPrenotazioneService{

	@Autowired
	LibroService libroService;
	@Autowired
	CopiaService copiaService;
	@Autowired
	PrenotazioneService prenotazioneService;
	
	public List<DomainPrenotazione> findByProprietario(String proprietario) {
		List<DomainPrenotazione> domainPrenotazioni = new ArrayList<DomainPrenotazione>();
		List<Prenotazione> prenotazioni = prenotazioneService.findByProprietario(proprietario);
		
		for(Prenotazione prenotazione : prenotazioni) {
			DomainPrenotazione domainPrenotazione  = new DomainPrenotazione();
			Copia copia = copiaService.findById(prenotazione.getIdCopia());
			Libro libro = libroService.findByISBN(copia.getIsbn());
			
			DomainLibro domainLibro  = new DomainLibro();
			domainLibro.setFromLibroAndCopia(libro, copia);
			
			domainPrenotazione.setPrenotazione(prenotazione);
			domainPrenotazione.setDomainLibro(domainLibro);
			domainPrenotazioni.add(domainPrenotazione);
		}
		
		return domainPrenotazioni;
	}
	
	
	public List<DomainPrenotazione> findByRichiedente(String richiedente) {
		List<DomainPrenotazione> domainPrenotazioni = new ArrayList<DomainPrenotazione>();
		List<Prenotazione> prenotazioni = prenotazioneService.findByRichiedente(richiedente);
		
		for(Prenotazione prenotazione : prenotazioni) {
			DomainPrenotazione domainPrenotazione  = new DomainPrenotazione();
			Copia copia = copiaService.findById(prenotazione.getIdCopia());
			Libro libro = libroService.findByISBN(copia.getIsbn());
			
			DomainLibro domainLibro  = new DomainLibro();
			domainLibro.setFromLibroAndCopia(libro, copia);
			
			domainPrenotazione.setPrenotazione(prenotazione);
			domainPrenotazione.setDomainLibro(domainLibro);
			domainPrenotazioni.add(domainPrenotazione);
		}
		
		return domainPrenotazioni;
	}
}

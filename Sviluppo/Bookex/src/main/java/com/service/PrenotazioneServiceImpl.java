package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.PrenotazioneDao;
import com.model.Prenotazione;

@Service
public class PrenotazioneServiceImpl implements PrenotazioneService{

	@Autowired 
	PrenotazioneDao prenotazioneDao;
	
	@Override
	public void insert(Prenotazione prenotazione) {
		prenotazioneDao.insert(prenotazione);
	}

	@Override
	public void update(Prenotazione prenotazione) {
		prenotazioneDao.update(prenotazione);
	}

	@Override
	public void delete(String id) {
		prenotazioneDao.delete(id);
	}
	
	@Override
	public void deleteAll() {
		prenotazioneDao.deleteAll();
	}

	@Override
	public Prenotazione findByIdCopia(String id) {
		return prenotazioneDao.findByIdCopia(id);
	}

	@Override
	public  List<Prenotazione> findByRichiedente(String richiedente) {
		return prenotazioneDao.findByRichiedente(richiedente);
	}
	
	@Override
	public  List<Prenotazione> findByProprietario(String proprietario) {
		return prenotazioneDao.findByProprietario(proprietario);
	}
}

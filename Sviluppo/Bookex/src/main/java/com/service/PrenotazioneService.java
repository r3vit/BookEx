package com.service;

import java.util.List;

import com.model.Prenotazione;

public interface PrenotazioneService {

	public void insert(Prenotazione prenotazione);
	
	public void update(Prenotazione prenotazione);
	
	public void delete(String id);
	
	public void deleteAll();
	
	public Prenotazione findByIdCopia(String id);
	
	public  List<Prenotazione> findByRichiedente(String richiedente);

	public  List<Prenotazione> findByProprietario(String proprietario);
}

package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.LibroDao;
import com.model.Libro;

@Service
public class LibroServiceImpl implements LibroService {

	@Autowired 
	LibroDao libroDao;
	
	public void insert(Libro libro) {
		libroDao.insert(libro);
	}
	
	public Libro findByISBN(String isbn) {
		return libroDao.findByISBN(isbn);
	}
	
	public void deleteAll() {
		libroDao.deleteAll();
	}
}

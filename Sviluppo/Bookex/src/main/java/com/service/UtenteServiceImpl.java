package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UtenteDao;
import com.model.Utente;

@Service
public class UtenteServiceImpl implements UtenteService {

	@Autowired 
	UtenteDao utenteDao;
	
	@Override
	public Utente findByUsername(String username) {
		return utenteDao.findByUsername(username);
	}
	
	public void update(Utente utente) {
		utenteDao.update(utente);
	}
}

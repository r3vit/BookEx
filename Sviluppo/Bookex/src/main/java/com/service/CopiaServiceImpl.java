package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.CopiaDao;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.model.Copia;
import com.model.DomainLibro;
import com.model.Libro;
import com.model.Utente;
import com.repository.LibroRepo;

@Service
public class CopiaServiceImpl implements CopiaService {

	@Autowired 
	CopiaDao copiaDao;
	@Autowired
	LibroService libroService;
	@Autowired
	CopiaService copiaService;
	@Autowired
	LibroRepo libroRepo;
	
	
	public void insert(Copia libro) {
		copiaDao.insert(libro);
	}

	
	public void update(Copia libro) {
		copiaDao.update(libro);
	}

	
	public void delete(String id) {
		copiaDao.delete(id);
	}

	
	public void deleteAll() {
		copiaDao.deleteAll();
	}
	
	
	public List<Copia>  findByPropetario(String proprietario) {
		return copiaDao.findByPropetario(proprietario);
	}
	
	
	public Copia findById(String id) {
		return copiaDao.findById(id);
	}
	
	
	public List<Copia> findByISBN(String isbn) {
		return copiaDao.findByISBN(isbn);
	}
	
	
	public String searchCopies(String query, Utente user) {
		String out = ""; 
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode array = mapper.createArrayNode();
		if (query.matches(".*[a-zA-Z]+.*")) {
    		List<Libro> libriSolr = libroRepo.findByTitolo(query);
    		for(Libro libroSolr : libriSolr) {
    			Libro libro = libroService.findByISBN(libroSolr.getIsbn());
    			List<Copia> copie = copiaDao.findByISBN(libro.getIsbn());
    			for(Copia copia : copie) {
    				if(!copia.getProprietario().equals(user.getUsername())) {
    					DomainLibro domainLibro = new DomainLibro();
    					domainLibro.setFromLibroAndCopia(libro, copia);
    					JsonNode node = mapper.convertValue(domainLibro, JsonNode.class);
    					array.add(node);
    				}
    			}
    		}
		}
		else {
			query = query.replace("-", "");
			query = query.replace(" ", "");
			System.out.println(query);
			if(checkValidity(query)) {
	    		List<Copia> copie = copiaService.findByISBN(query);
	    		Libro libro = libroService.findByISBN(query);
	    		for(Copia copia : copie) {
					if(!copia.getProprietario().equals(user.getUsername())) {
		    			DomainLibro domainLibro = new DomainLibro();
		    			domainLibro.setFromLibroAndCopia(libro, copia);
		    			JsonNode node = mapper.convertValue(domainLibro, JsonNode.class);
						array.add(node);
					}
	    		}
			}
			else {
				System.out.println("INVALIDOOOO");
			}
    	}
		try { out = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(array);}
		catch(Exception e) {e.printStackTrace(); }
		
		return "{ \"copie\": " + out + "}";
	}

	
	public boolean checkValidity(String isbn){
		if(isbn.length() == 10) {
			int[] ints = new int[10];
			for(int i=0; i<10; i++) 
				ints[i] =  Integer.parseInt(isbn.substring(i, i+1));
			int sum = ints[0]*10 + ints[1]*9 + ints[2]*8 + ints[3]*7 + ints[4]*6 + ints[5]*5 + 
				      ints[6]*4 + ints[7]*3 + ints[8]*2 + ints[9]*1;
			if(sum%11 == 0)
				return true; 
			else 
				return false;
		}
		else if (isbn.length() == 13) {
			int[] ints = new int[13];
			for(int i=0; i<13; i++) 
				ints[i] =  Integer.parseInt(isbn.substring(i, i+1));
			int sum = ints[0]*1 + ints[1]*3 + ints[2]*1 + ints[3]*3 + ints[4]*1 + ints[5]*3 + ints[6]*1 + 
					  ints[7]*3 + ints[8]*1 + ints[9]*3 + ints[10]*1 + ints[11]*3;
			int control = 10 - (sum % 10);
			if(control == 10)
				control = 0;
			if(control == ints[12])
				return true;
			else 
				return false;		
		}
		else
			return false;
	}
	
}

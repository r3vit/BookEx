package com.service;

import com.model.Utente;

public interface UtenteService {
	
	public Utente findByUsername(String username);
	
	public void update(Utente utente);
}

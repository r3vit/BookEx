package com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Copia;
import com.model.DomainLibro;
import com.model.Libro;

@Service
public class DomainLibroServiceImpl implements DomainLibroService{

	@Autowired
	LibroService libroService;
	@Autowired
	CopiaService copiaService;

	public List<DomainLibro> findByProprietario(String proprietario) {
		List<DomainLibro> domainLibri = new ArrayList<DomainLibro>();
		
		List<Copia> copie = copiaService.findByPropetario(proprietario);
		for(Copia copia: copie) {
			String isbn = copia.getIsbn();
			Libro libro = libroService.findByISBN(isbn);
			
			DomainLibro domainLibro  = new DomainLibro();
			domainLibro.setFromLibroAndCopia(libro, copia);
			domainLibri.add(domainLibro);
		}
		return domainLibri;
	}
}

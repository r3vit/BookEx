package com.service;

import java.util.List;

import com.model.Copia;
import com.model.Utente;

public interface CopiaService {

	public void insert(Copia libro);
	
	public void update(Copia libro);
	
	public void delete(String  id);
	
	public void deleteAll();
	
	public List<Copia>  findByPropetario(String proprietario);
	
	public Copia findById(String id);
	
	public  List<Copia> findByISBN(String isbn);
	
	//public String findByISBNJSON(String isbn);
	
	public String searchCopies(String query, Utente user);

	
}

package com.service;

import com.model.Libro;

public interface LibroService {

	public void insert(Libro libro);
	
	public Libro findByISBN(String isbn);
	
	public void deleteAll();
}

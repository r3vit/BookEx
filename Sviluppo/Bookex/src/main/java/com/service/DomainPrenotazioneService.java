package com.service;

import java.util.List;

import com.model.DomainPrenotazione;

public interface DomainPrenotazioneService {

	public List<DomainPrenotazione> findByProprietario(String proprietario);
	
	public List<DomainPrenotazione> findByRichiedente(String richiedente);
}

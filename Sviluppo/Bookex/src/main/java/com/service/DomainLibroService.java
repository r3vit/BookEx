package com.service;

import java.util.List;

import com.model.DomainLibro;

public interface DomainLibroService {

	public List<DomainLibro> findByProprietario(String proprietario);
}

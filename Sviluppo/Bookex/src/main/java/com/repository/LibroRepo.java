package com.repository;

import java.util.List;

import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Component;

import com.model.Libro;



@Component
public interface LibroRepo extends SolrCrudRepository<Libro, Long>{

	void deleteByIsbn(String isbn);

	List<Libro> findByTitolo(String titolo);
}

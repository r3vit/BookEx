package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.model.Copia;
import com.model.DomainLibro;
import com.model.Utente;
import com.service.CopiaService;
import com.service.DomainLibroService;
import com.service.PrenotazioneService;
import com.service.UtenteService;

@Controller
public class BookController {

	@Autowired
	DomainLibroService domainLibroService;
	@Autowired
	CopiaService copiaService;
	@Autowired
	PrenotazioneService prenotazioneService;
	@Autowired
	UtenteService utenteService;
	
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") String id) throws Exception{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String user = auth.getName(); //get logged in username
	    Copia copia = copiaService.findById(id);
	    Utente proprietario = utenteService.findByUsername(copia.getProprietario());
	    String stato = copia.getStato();
	    
	    if(user.contentEquals(proprietario.getUsername()) && stato.equals("disponibile")) {
	    	proprietario.setPunti(proprietario.getPunti() - 1);
	    	copiaService.delete(id);
	    	utenteService.update(proprietario);
	    }
	   
		return "redirect:../mybooks";
	}

	
	@RequestMapping(value = "/mybooks", method = RequestMethod.GET)
	public ModelAndView myBooks() throws Exception{
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String user = auth.getName();
		ModelAndView model = new ModelAndView();
		model.setViewName("myBooks");
		
		List<DomainLibro> domainLibri = domainLibroService.findByProprietario(user);
		if(domainLibri.size()>0) 
			model.addObject("domainLibri", domainLibroService.findByProprietario(user));
		else
			model.addObject("message","La tua libreria è vuota. Aggiungi qualche libro!");
      
		model.addObject("userData", utenteService.findByUsername(user));
		return model;
	}
	
	
	@RequestMapping(value = "/booklist/{user}", method = RequestMethod.GET)
	public ModelAndView bookList(@PathVariable("user") String user) throws Exception{
		ModelAndView model = new ModelAndView();
		model.setViewName("bookList");
		model.addObject("bookList", copiaService.findByPropetario(user));
		
		return model;
	}
	
}

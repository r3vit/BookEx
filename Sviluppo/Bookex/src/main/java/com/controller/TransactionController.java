package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.model.Copia;
import com.model.DomainPrenotazione;
import com.model.Prenotazione;
import com.model.Utente;
import com.service.CopiaService;
import com.service.DomainPrenotazioneService;
import com.service.LibroService;
import com.service.PrenotazioneService;
import com.service.UtenteService;

@Controller
public class TransactionController {
	@Autowired
	CopiaService copiaService;
	@Autowired
	LibroService libroService;
	@Autowired
	PrenotazioneService prenotazioneService;
	@Autowired
	UtenteService utenteService;
	@Autowired
	DomainPrenotazioneService domainPrenotazioneService;

	@RequestMapping(value = "/mytransactions", method = RequestMethod.GET)
	public ModelAndView myTransactions() {
		ModelAndView model = new ModelAndView();
		model.setViewName("mytransactions");
		Utente user = getRequestingUser();

		List<DomainPrenotazione> domainPrenotazioniRichiedente = domainPrenotazioneService.findByRichiedente(user.getUsername());
		if(domainPrenotazioniRichiedente.size() > 0)
			model.addObject("domainPrenotazioni_richiedente", domainPrenotazioniRichiedente);
		else
			model.addObject("message1", "Non hai ancora prenotato nessun libro");
		
		List<DomainPrenotazione> domainPrenotazioniProprietario = domainPrenotazioneService.findByProprietario(user.getUsername());
		if(domainPrenotazioniProprietario.size() > 0)
			model.addObject("domainPrenotazioni_proprietario", domainPrenotazioniProprietario);
		else
			model.addObject("message2", "Non hai richieste di prenotazione");

		model.addObject("userData", user);
		return model;
	}

	@RequestMapping(value = "/reserve/{id}", method = RequestMethod.POST)
	public Object reserve(@PathVariable("id") String id) throws Exception {
		Utente user = getRequestingUser();
		Copia copia = copiaService.findById(id);

		ModelAndView model = new ModelAndView();
		
		if (copia.getStato().equals("disponibile") && user.getPunti() >= copia.getValorePunti()) {
			copia.setStato("non_disponibile");
			copiaService.update(copia);

			Prenotazione prenotazione = new Prenotazione();
			prenotazione.setIdCopia(id);
			prenotazione.setRichiedente(user.getUsername());
			prenotazione.setProprietario(copia.getProprietario());
			prenotazione.setStato("prenotazione_richiesta");

			user.setPunti(user.getPunti() - copia.getValorePunti());
			user.setPuntiImpegnati(user.getPuntiImpegnati() + copia.getValorePunti());

			prenotazioneService.insert(prenotazione);
			utenteService.update(user);
		}
		
		if(user.getPunti() < copia.getValorePunti()) {
			model.setViewName("search");
			model.addObject("message","Non hai abbastanza punti per richiedere questo libro!");
			return model;	
		}
		
		return "redirect:../mytransactions";
	}

	@RequestMapping(value = "/aceptreservation/{id}", method = RequestMethod.POST)
	public String acceptReservation(@PathVariable("id") String id) throws Exception {
		Utente user = getRequestingUser();
		Prenotazione prenotazione = prenotazioneService.findByIdCopia(id);
		Utente proprietario = utenteService.findByUsername(prenotazione.getProprietario());

		if (user.getUsername().equals(proprietario.getUsername())) {
			prenotazione.setStato("prenotazione_accettata");
			prenotazioneService.update(prenotazione);
		}
		return "redirect:../mytransactions";
	}

	@RequestMapping(value = "/cancelreservation/{id}", method = RequestMethod.POST)
	public String cancelReservation(@PathVariable("id") String id) throws Exception {
		Utente user = getRequestingUser();
		Prenotazione prenotazione = prenotazioneService.findByIdCopia(id);
		Utente proprietario = utenteService.findByUsername(prenotazione.getProprietario());
		Utente richiedente = utenteService.findByUsername(prenotazione.getRichiedente());
		Copia copia = copiaService.findById(id);

		if (prenotazione.getStato().equals("prenotazione_richiesta")) {
			if (user.getUsername().equals(proprietario.getUsername())
					|| user.getUsername().equals(richiedente.getUsername())) {
				prenotazioneService.delete(id);
				copia.setStato("disponibile");
				richiedente.setPuntiImpegnati(richiedente.getPuntiImpegnati() - copia.getValorePunti());
				richiedente.setPunti(richiedente.getPunti() + copia.getValorePunti());
			}
		} else if (prenotazione.getStato().equals("prenotazione_accettata")) {
			if (user.getUsername().equals(proprietario.getUsername())) {
				prenotazioneService.delete(id);
				copia.setStato("disponibile");
				richiedente.setPuntiImpegnati(richiedente.getPuntiImpegnati() - copia.getValorePunti());
				richiedente.setPunti(richiedente.getPunti() + copia.getValorePunti());
			}
		}
		copiaService.update(copia);
		utenteService.update(richiedente);

		return "redirect:../mytransactions";
	}

	@RequestMapping(value = "/sendbook/{id}", method = RequestMethod.POST)
	public String sendBook(@PathVariable("id") String id) {
		Utente user = getRequestingUser();
		Prenotazione prenotazione = prenotazioneService.findByIdCopia(id);
		Utente proprietario = utenteService.findByUsername(prenotazione.getProprietario());

		if (prenotazione.getStato().equals("prenotazione_accettata")
				&& user.getUsername().equals(proprietario.getUsername())) {
			prenotazione.setStato("spedito");
			prenotazioneService.update(prenotazione);
		}

		return "redirect:../mytransactions";
	}

	@RequestMapping(value = "/bookreceived/{id}", method = RequestMethod.POST)
	public String bookRescieved(@PathVariable("id") String id) {
		Utente user = getRequestingUser();
		Copia copia = copiaService.findById(id);
		Prenotazione prenotazione = prenotazioneService.findByIdCopia(id);
		Utente richiedente = utenteService.findByUsername(prenotazione.getRichiedente());

		if (prenotazione.getStato().equals("spedito") && user.getUsername().equals(richiedente.getUsername())) {
			prenotazione.setStato("ricevuto");
			richiedente.setPuntiImpegnati(richiedente.getPuntiImpegnati() - copia.getValorePunti());

			prenotazioneService.update(prenotazione);
			utenteService.update(richiedente);
		}

		return "redirect:../mytransactions";
	}

	@RequestMapping(value = "/booknotreceived/{id}", method = RequestMethod.POST)
	public String bookNoptRescieved(@PathVariable("id") String id) {
		Utente user = getRequestingUser();
		Copia copia = copiaService.findById(id);
		Prenotazione prenotazione = prenotazioneService.findByIdCopia(id);
		Utente richiedente = utenteService.findByUsername(prenotazione.getRichiedente());

		if (prenotazione.getStato().equals("spedito") && user.getUsername().equals(richiedente.getUsername())) {
			prenotazione.setStato("non_ricevuto");
			richiedente.setPuntiImpegnati(richiedente.getPuntiImpegnati() - copia.getValorePunti());
			richiedente.setPunti(richiedente.getPunti() + copia.getValorePunti());

			prenotazioneService.update(prenotazione);
			utenteService.update(richiedente);
		}

		return "redirect:../mytransactions";
	}

	@RequestMapping(value = "/ok/{id}", method = RequestMethod.POST)
	public String ok(@PathVariable("id") String id) {
		Utente user = getRequestingUser();
		Copia copia = copiaService.findById(id);
		Prenotazione prenotazione = prenotazioneService.findByIdCopia(id);
		Utente proprietario = utenteService.findByUsername(prenotazione.getProprietario());

		if ((prenotazione.getStato().equals("ricevuto") || prenotazione.getStato().equals("non_ricevuto"))
				&& user.getUsername().equals(proprietario.getUsername())) {
			prenotazioneService.delete(prenotazione.getIdCopia());
			proprietario.setPunti(proprietario.getPunti() + copia.getValorePunti());

			copiaService.delete(id);
			utenteService.update(proprietario);
		}

		return "redirect:../mytransactions";
	}

	private Utente getRequestingUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return utenteService.findByUsername(auth.getName());
	}
}

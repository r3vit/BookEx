package com.controller;

import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.model.DomainLibro;
import com.model.Libro;
import com.model.Utente;
import com.repository.LibroRepo;
import com.model.Copia;
import com.service.CopiaService;
import com.service.LibroService;
import com.service.UtenteService;

@Controller
@SessionAttributes("domainLibro")
public class AddBookController {
	static final String googleAPI = "https://www.googleapis.com/books/v1/volumes";

	@Autowired
	LibroService libroService;
	@Autowired
	CopiaService copiaService;
	@Autowired
	UtenteService utenteService;
	@Autowired
	LibroRepo libroRepo;
	
	
	@RequestMapping(value = "/addbook", method = RequestMethod.GET)
	public ModelAndView addBook() {
		Utente user = getRequestingUser();
		ModelAndView model = new ModelAndView();
		model.setViewName("addBook");
		
		model.addObject("userData", user);
		return model;
	}

	
	@RequestMapping(value = "/addbookresults", method = RequestMethod.GET)
	public ModelAndView addBookResults(@RequestParam(value="query", required=true) String query) throws Exception{
		Utente user = getRequestingUser();
		ModelAndView model = new ModelAndView();
		model.setViewName("addBookResults");
		
		List<DomainLibro> domainLibri = new ArrayList<DomainLibro>();
		if (query.matches(".*[a-zA-Z]+.*")) {
			List<String> selfLinks = obtainSelfLink( "?q=" +  URLEncoder.encode(query).replace("+", "%20"));
			if(!selfLinks.isEmpty()) {
				for(String selfLink : selfLinks) {
					DomainLibro domainLibro = getDataFromGoogle(selfLink, query);
					if(domainLibro!=null) //per evitare libri senza isbn
						domainLibri.add(domainLibro);
				}
			}
			else {
				model.setViewName("addBook");
				model.addObject("message","Libro non trovato. Provane un altro oppure inseriscilo manualmente");
			}
		}
		else {
			query = query.replace("-", "");
			query = query.replace(" ", "");

			List<String> selfLinks = obtainSelfLink("?q=isbn:" + query);
			if(!selfLinks.isEmpty()) {
				DomainLibro domainLibro = getDataFromGoogle(selfLinks.get(0), query);
				domainLibri.add(domainLibro);
			}
			else {
				model.setViewName("addBook");
				model.addObject("message","Libro non trovato. Provane un altro oppure inseriscilo manualmente");
			}
		}
		model.addObject("domainLibri",domainLibri);
		model.addObject("userData", user);
		return model;
	}
	
	
	@RequestMapping(value = "/addbookenrich", method = RequestMethod.GET)
	public ModelAndView addBookEnrich(@RequestParam(value="isbn", required=true) String isbn) throws Exception{
		Utente user = getRequestingUser();
        ModelAndView model = new ModelAndView();
		model.setViewName("addBookEnrich");
		
		List<String> selfLinks = obtainSelfLink("?q=isbn:" + isbn);
		DomainLibro domainLibro = getDataFromGoogle(selfLinks.get(0), isbn);
		model.addObject("domainLibro",domainLibro);

        model.addObject("domainLibro", domainLibro);
        model.addObject("userData", user);
		return model;
	}
	
	
	@RequestMapping(value = "/addbookfromscratch", method = RequestMethod.GET)
	public ModelAndView addBookFromScratch() throws Exception{
		Utente user = getRequestingUser();
        ModelAndView model = new ModelAndView();
		model.setViewName("addBookFromScratch");
		
        model.addObject("domainLibro", new DomainLibro());
        model.addObject("userData",user);
		return model;
	}
	
	
	@RequestMapping(value = "/submitbook", method = RequestMethod.POST)
	public String submitBook(@ModelAttribute("domainLibro") DomainLibro domainLibro) throws Exception {
		String name = getRequestingUser().getUsername();
	    domainLibro.setProprietario(name);
	    domainLibro.setStato("disponibile");
	   
	    Libro libro = new Libro();
	    libro.setFromDomainLibro(domainLibro);
	    Copia copia = new Copia();
	    copia.setFromDomainLibro(domainLibro);
	    Utente utente = utenteService.findByUsername(name);
	    utente.setPunti(utente.getPunti() + 1);
	    
	    libroService.insert(libro);
	    copiaService.insert(copia);
	    if(libroRepo.findByTitolo(libro.getTitolo()).size() == 0)
	    	libroRepo.save(libro);
	    utenteService.update(utente);
		
		return "redirect:";
	}
	
	public List<String> obtainSelfLink(String query) throws Exception{
		List<String> list = new ArrayList<String>();
		InputStream stream = new URL(googleAPI + query).openStream();
		Object obj = new JSONParser().parse(IOUtils.toString(stream, StandardCharsets.UTF_8));
		JSONObject json = (JSONObject) obj;
		JSONArray items = (JSONArray) json.get("items");
		if(items != null){
			for(int i=0; i<items.size(); i++) {
				JSONObject item = (JSONObject) items.get(i);
				list.add(item.get("selfLink").toString());
			}
		}
		return list;
	}

	
	public DomainLibro getDataFromGoogle(String selfLink, String query) throws Exception {
		InputStream stream = new URL(selfLink).openStream();
		Object obj = new JSONParser().parse(IOUtils.toString(stream, StandardCharsets.UTF_8));
		JSONObject json = (JSONObject) obj;
		JSONObject volumeInfo = (JSONObject) json.get("volumeInfo");
		JSONArray isbns = (JSONArray) volumeInfo.get("industryIdentifiers");
		if(isbns == null)
			return null;
	
		DomainLibro domainLibro = new DomainLibro();
		
		if (query.matches(".*[a-zA-Z]+.*")) {     //se query è un titolo
			JSONObject isbnobj = (JSONObject) isbns.get(0);
			domainLibro.setIsbn(isbnobj.get("identifier").toString());
		}
		else
			domainLibro.setIsbn(query);
		
		if(volumeInfo.get("title")!=null)
			domainLibro.setTitolo(volumeInfo.get("title").toString());
		else
			domainLibro.setTitolo("-");
		if(volumeInfo.get("authors")!=null)
			domainLibro.setAutori(volumeInfo.get("authors").toString());
		else
			domainLibro.setAutori("-");
		if(volumeInfo.get("publisher") != null)
			domainLibro.setEditore(volumeInfo.get("publisher").toString());
		else
			domainLibro.setEditore("-");
		if(volumeInfo.get("publishedDate") != null)
			domainLibro.setAnnoPubblicazione(volumeInfo.get("publishedDate").toString());
		else
			domainLibro.setAnnoPubblicazione("-");
		if(volumeInfo.get("categories")!=null)
			domainLibro.setGeneri(volumeInfo.get("categories").toString());
		else
			domainLibro.setGeneri("-");
		if(volumeInfo.get("language") !=null)
			domainLibro.setLingua(volumeInfo.get("language").toString());
		else
			domainLibro.setLingua("-");
		if(volumeInfo.get("pageCount") !=null)
			domainLibro.setNumPagine(Integer.parseInt(volumeInfo.get("pageCount").toString()));
		else
			domainLibro.setNumPagine(0);
		if(volumeInfo.get("description") !=null)
			domainLibro.setDescrizione(volumeInfo.get("description").toString());
		else
			domainLibro.setDescrizione("-");
		if(volumeInfo.get("imageLinks") !=null) {
			JSONObject images = (JSONObject) volumeInfo.get("imageLinks");
			if(images.get("thumbnail") !=null)
				domainLibro.setImg(images.get("thumbnail").toString());
		}
		else
			domainLibro.setImg("");
		
		return domainLibro;
	}
	
	
	private Utente getRequestingUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return utenteService.findByUsername(auth.getName()); 
	}

}

package com.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.service.CopiaService;
import com.service.PrenotazioneService;
import com.service.UtenteService;

@Controller
public class IndexController {

	@Autowired
	UtenteService utenteService;
	@Autowired
	CopiaService copiaService;
	@Autowired
	PrenotazioneService prenotazioneService;
	

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error, @RequestParam(value = "logout", required = false) String logout) {
		ModelAndView model = new ModelAndView();
		if (error != null) 
			model.addObject("error", "Username o password errati");

		if (logout != null) 
			model.addObject("msg", "Sei uscito correttamente dalla sessione");
		
		model.setViewName("login");
		return model;
	}
	
	
	@RequestMapping(value={ "/", "/myprofile", "/home"} , method = RequestMethod.GET)
	public ModelAndView myprofile(){
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String user = auth.getName();
		ModelAndView model = new ModelAndView();
		model.setViewName("myprofile");
		model.addObject("userData", utenteService.findByUsername(user));
		
		return model;
	}
	

	//for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {
		ModelAndView model = new ModelAndView();
	
		//check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}
		model.setViewName("403");
		return model;
	}
}

package com.controller;


import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.model.DomainLibro;
import com.model.Utente;
import com.repository.LibroRepo;
import com.service.CopiaService;
import com.service.LibroService;
import com.service.UtenteService;

@Controller
public class SearchBookController {

	@Autowired
	LibroService libroService;
	@Autowired
	CopiaService copiaService;
	@Autowired
	UtenteService utenteService;
	@Autowired
	LibroRepo libroRepo;
	
	
	
	@RequestMapping(value="/api/v1/search", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String browse(
			@RequestParam(value="query", required=true) String query) {
		Utente user = getRequestingUser();
		String json  = copiaService.searchCopies(query, user);
		return json;
	}
	
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView search() {
		Utente user = getRequestingUser(); 
		ModelAndView model = new ModelAndView();
		model.setViewName("search");
		
		model.addObject("userData", user);
		return model;
	}
	
	
	@RequestMapping(value = "/searchresults", method = RequestMethod.GET)
	public ModelAndView searchResults(@RequestParam(value="query", required=true) String query) throws Exception{
		Utente user = getRequestingUser();
		ModelAndView model = new ModelAndView();
		model.setViewName("searchResults");
        
        List<DomainLibro> domainLibri = new ArrayList<DomainLibro>();
        
        String jsonString  = copiaService.searchCopies(query, user);
		Object obj = new JSONParser().parse(jsonString);
		JSONObject json = (JSONObject) obj;
		JSONArray copie = (JSONArray) json.get("copie");
		
		if(!copie.isEmpty()) {
			for(int i=0 ; i<copie.size(); i++) {
				JSONObject doc = (JSONObject) copie.get(i);
				DomainLibro domainLibro = new DomainLibro();
				domainLibro.setFromJSONObject(doc);
				domainLibri.add(domainLibro);
			}
			 model.addObject("domainLibri", domainLibri);
		}
		else {
			model.setViewName("search");
			model.addObject("message","Libro non trovato");
		}
		
		model.addObject("userData", user);
		return model;
	}
	
	
	private Utente getRequestingUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return utenteService.findByUsername(auth.getName()); 
	}
	
}

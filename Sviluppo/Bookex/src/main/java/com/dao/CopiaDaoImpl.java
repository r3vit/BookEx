package com.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.model.Copia;

@Repository
public class CopiaDaoImpl implements CopiaDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static final String table = "copie";

	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) { 
		this.jdbcTemplate = jdbcTemplate; 
	}
	
	
	public void insert(Copia  copia) {
		jdbcTemplate.update("insert into " + table + " (id, isbn, proprietario, annoAcquisto, statoCopertina, isSottolineato, pagineIngiallite, pagineIlleggibili, commenti, stato, valorePunti) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				new Object[] {copia.getId(), copia.getIsbn(), copia.getProprietario(), copia.getAnnoAcquisto(), copia.getStatoCopertina(), copia.getIsSottolienato(), copia.getPagineIngiallite(), copia.getPagineIlleggibili(), copia.getCommenti(), copia.getStato(), copia.getValorePunti()});
	}

	
	public void update(Copia copia) {
		 String updateStatement = " UPDATE " + table 
                + " SET isbn=?, proprietario=?, annoAcquisto=?, statoCopertina=?, isSottolineato=?, pagineIngiallite=?, pagineIlleggibili=?, commenti=?, stato=?, valorePunti=?"
                + " WHERE id=?";
		 jdbcTemplate.update(updateStatement, new Object[] {copia.getIsbn(), copia.getProprietario(), copia.getAnnoAcquisto(), copia.getStatoCopertina(), copia.getIsSottolienato(), copia.getPagineIngiallite(), copia.getPagineIlleggibili(), copia.getCommenti(), copia.getStato(), copia.getValorePunti(), copia.getId()});
	}

	
	public void delete(String id) {
		jdbcTemplate.update("delete from " + table + " where id = ?", 
				new Object[] {id} );
	}
	
	public void deleteAll() {
		jdbcTemplate.update("delete from " + table );
	}
	

	public Copia findById(String id) {
		Copia book;
		try {
		book = (Copia) jdbcTemplate.queryForObject("select * from " + table + " where id = ?",
				new Object[] {id},
				new CopiaRowMapper());
		}
		catch(Exception e) {
			return null;
		}
		return book;
	}
	
	
	public List<Copia>  findByISBN(String isbn) {	
		List<Copia> libri = new ArrayList<Copia>();
		List<Map<String,Object>> rows = jdbcTemplate.queryForList("select * from " + table + " where isbn = ?", new Object[]{isbn});
		
		for(Map<String, Object> row : rows) {
			Copia copia = new Copia();
			copia.setId((String)row.get("id"));
			copia.setIsbn((String)row.get("isbn"));
			copia.setProprietario((String)row.get("proprietario"));
			copia.setAnnoAcquisto((String)row.get("annoAcquisto"));
			copia.setStatoCopertina((String)row.get("statoCopertina"));
			copia.setIsSottolienato((String)row.get("isSottolineato"));
			copia.setPagineIngiallite((String)row.get("pagineIngiallite"));
			copia.setPagineIlleggibili((String)row.get("pagineIlleggibili"));
			copia.setCommenti((String)row.get("commenti"));
			copia.setStato((String)row.get("stato"));
			copia.setValorePunti((Integer)row.get("valorePunti"));
			libri.add(copia);
		}
		return libri;
	}
	
	
	
	public List<Copia>  findByPropetario(String proprietario) {	
		List<Copia> libri = new ArrayList<Copia>();
		List<Map<String,Object>> rows = jdbcTemplate.queryForList("select * from " + table + " where proprietario = ?", new Object[]{proprietario});
		
		for(Map<String, Object> row : rows) {
			Copia copia = new Copia();
			copia.setId((String)row.get("id"));
			copia.setIsbn((String)row.get("isbn"));
			copia.setProprietario((String)row.get("proprietario"));
			copia.setAnnoAcquisto((String)row.get("annoAcquisto"));
			copia.setStatoCopertina((String)row.get("statoCopertina"));
			copia.setIsSottolienato((String)row.get("isSottolineato"));
			copia.setPagineIngiallite((String)row.get("pagineIngiallite"));
			copia.setPagineIlleggibili((String)row.get("pagineIlleggibili"));
			copia.setCommenti((String)row.get("commenti"));
			copia.setStato((String)row.get("stato"));
			copia.setValorePunti((Integer)row.get("valorePunti"));
			libri.add(copia);
		}
		return libri;
	}
	
}

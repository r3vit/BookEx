package com.dao;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.model.Prenotazione;

@Repository
public interface PrenotazioneDao {

	public void insert(Prenotazione prenotazione);
	
	public void update(Prenotazione prenotazione);
	
	public void delete(String id);
	
	public void deleteAll();
	
	
	public Prenotazione findByIdCopia(String id);

	public  List<Prenotazione> findByRichiedente(String richiedente);

	public  List<Prenotazione> findByProprietario(String proprietario);
}

package com.dao;

import com.model.Libro;

public interface LibroDao {

	public void insert(Libro libro);
	
	public Libro findByISBN(String isbn);
	
	public void deleteAll();
}

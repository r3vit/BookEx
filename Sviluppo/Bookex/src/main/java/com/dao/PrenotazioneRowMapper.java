package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.model.Prenotazione;

public class PrenotazioneRowMapper implements RowMapper {
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Prenotazione prenotazione = new Prenotazione();
		prenotazione.setIdCopia(rs.getString("idCopia"));
		prenotazione.setRichiedente(rs.getString("richiedente"));
		prenotazione.setProprietario(rs.getString("proprietario"));
		prenotazione.setStato(rs.getString("stato"));
		
		return prenotazione;
	}
}

package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.model.Libro;


public class LibroRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Libro libro = new Libro();
		libro.setIsbn(rs.getString("isbn"));
		libro.setTitolo(rs.getString("titolo"));
		libro.setAutori(rs.getString("autori"));
		libro.setEditore(rs.getString("editore"));
		libro.setAnnoPubblicazione(rs.getString("annoPubblicazione"));
		libro.setGeneri(rs.getString("generi"));
		libro.setLingua(rs.getString("lingua"));
		libro.setNumPagine(Integer.parseInt(rs.getString("numPagine")));
		libro.setDescrizione(rs.getString("descrizione"));
		libro.setImg(rs.getString("img"));
		
		return libro;
	}
	

}

package com.dao;

import java.util.List;

import com.model.Copia;

public interface CopiaDao {

	public void insert(Copia copia);
	
	public void update(Copia copia);
	
	public void delete(String  id);
	
	public void deleteAll();
	
	public List<Copia>  findByPropetario(String proprietario);

	public Copia findById(String id);
	
	public List<Copia> findByISBN(String isbn);
}

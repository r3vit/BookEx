package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.model.Utente;

@Repository
public class UtenteDaoImpl implements UtenteDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static final String table = "users";
	
	public Utente findByUsername(String username) {
		Utente utente;
		try {
		utente = (Utente) jdbcTemplate.queryForObject("select * from " + table + " where username = ?",
				new Object[] {username},
				new UtenteRowMapper());
		}
		catch(Exception e) {
			return null;
		}
		return utente;
	}
	
	
	public void update(Utente utente) {
		 String updateStatement = " UPDATE " + table 
               + " SET password=?, nome=?, cognome=?, sesso=?, indirizzo=?, citta=?, provincia=?, cap=?, telefono=?, email=?, punti=?, puntiImpegnati=? "
               + " WHERE username=?";
		 jdbcTemplate.update(updateStatement, new Object[] {utente.getPassword(), utente.getNome(), utente.getCognome(), utente.getSesso(), utente.getIndirizzo(), utente.getCitta(), utente.getProvincia(), utente.getCap(), utente.getTelefono(), utente.getEmail(), utente.getPunti(), utente.getPuntiImpegnati(), utente.getUsername()});
	}
	
	
}

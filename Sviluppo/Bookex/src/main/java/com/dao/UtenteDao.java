package com.dao;

import com.model.Utente;

public interface UtenteDao {

	public Utente findByUsername(String username);
	
	public void update(Utente utente);
}

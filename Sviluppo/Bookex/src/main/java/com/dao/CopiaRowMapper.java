package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.model.Copia;

public class CopiaRowMapper implements RowMapper {

	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Copia copia = new Copia();
		copia.setId(rs.getString("id"));
		copia.setIsbn(rs.getString("isbn"));
		copia.setProprietario(rs.getString("proprietario"));
		copia.setAnnoAcquisto(rs.getString("annoAcquisto"));
		copia.setStatoCopertina(rs.getString("statoCopertina"));
		copia.setIsSottolienato(rs.getString("isSottolineato"));
		copia.setPagineIngiallite(rs.getString("pagineIngiallite"));
		copia.setPagineIlleggibili(rs.getString("pagineIlleggibili"));
		copia.setCommenti(rs.getString("commenti"));
		copia.setStato(rs.getString("stato"));
		copia.setValorePunti(Integer.parseInt(rs.getString("valorePunti")));
		
		
		return copia;
	}
	

}

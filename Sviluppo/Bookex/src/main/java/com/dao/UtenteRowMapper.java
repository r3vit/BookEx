package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.model.Utente;


public class UtenteRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Utente utente = new Utente();
		utente.setUsername(rs.getString("username"));
		utente.setPassword(rs.getString("password"));
		utente.setNome(rs.getString("nome"));
		utente.setCognome(rs.getString("cognome"));
		utente.setSesso(rs.getString("sesso"));
		utente.setIndirizzo(rs.getString("indirizzo"));
		utente.setCitta(rs.getString("citta"));
		utente.setProvincia(rs.getString("provincia"));
		utente.setCap(rs.getString("cap"));
		utente.setTelefono(rs.getString("telefono"));
		utente.setEmail(rs.getString("email"));
		utente.setPunti(rs.getInt("punti"));
		utente.setPuntiImpegnati(rs.getInt("puntiImpegnati"));
		
		return utente;
	}
}

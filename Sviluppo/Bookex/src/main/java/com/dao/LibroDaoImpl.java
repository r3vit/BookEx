package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.model.Libro;

@Repository
public class LibroDaoImpl implements LibroDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static final String table = "libri";
	
	public void insert(Libro  libro) {
		jdbcTemplate.update("insert ignore into " + table + " (isbn, titolo, autori, editore, annoPubblicazione, generi, lingua, numPagine, descrizione, img) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				new Object[] {libro.getIsbn(), libro.getTitolo(), libro.getAutori(), libro.getEditore(), libro.getAnnoPubblicazione(), libro.getGeneri(), libro.getLingua(), libro.getNumPagine(), libro.getDescrizione(), libro.getImg()});
	}
	
	
	public Libro findByISBN(String isbn) {
		Libro libro;
		try {
			libro = (Libro) jdbcTemplate.queryForObject("select * from " + table + " where isbn = ?",
				new Object[] {isbn},
				new LibroRowMapper());
		}
		catch(Exception e) {
			return null;
		}
		return libro;
	}
	
	public void deleteAll() {
		jdbcTemplate.update("delete from " + table );
	}
	
}

package com.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.model.Prenotazione;

@Repository
public class PrenotazioneDaoImpl implements PrenotazioneDao{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private static final String table = "prenotazioni";
	
	@Override
	public void insert(Prenotazione prenotazione) {
		jdbcTemplate.update("insert into " + table + " (idCopia, richiedente, proprietario, stato) values (?, ?, ?, ?)",
				new Object[] {prenotazione.getIdCopia(), prenotazione.getRichiedente(), prenotazione.getProprietario(), prenotazione.getStato() });
	}

	
	@Override
	public void update(Prenotazione prenotazione) {
	//	jdbcTemplate.update("update " + table + " set richiedente = ?,  set propriterario = ?, stato = ? where isbn = ? ;", 
		//		new Object[] {prenotazione.getRichiedente(), prenotazione.getProprietario(), prenotazione.getStato(), prenotazione.getIsbn() });
		
		 String updateStatement = " UPDATE " + table 
                 + " SET richiedente=?,  proprietario=?, stato=? "
                 + " WHERE idCopia=?";
		 jdbcTemplate.update(updateStatement, new Object[] {prenotazione.getRichiedente(), prenotazione.getProprietario(), prenotazione.getStato(), prenotazione.getIdCopia() });
	}
	

	@Override
	public void delete(String id) {
		jdbcTemplate.update("delete from " + table + " where idCopia = ?", 
				new Object[] {id} );
		
	}
	
	
	public void deleteAll() {
		jdbcTemplate.update("delete from " + table );
	}
	

	@Override
	public Prenotazione findByIdCopia(String id) {
		Prenotazione prenotazione;
		try {
			prenotazione = (Prenotazione) jdbcTemplate.queryForObject("select * from " + table + " where idCopia = ?",
				new Object[] {id},
				new PrenotazioneRowMapper());
		}
		catch(Exception e) {
			return null;
		}
		return prenotazione;
	}

	public List<Prenotazione> findByRichiedente(String richiedente) {
		List<Prenotazione> prenotazioni = new ArrayList<Prenotazione>();
		List<Map<String,Object>> rows = jdbcTemplate.queryForList("select * from " + table + " where richiedente = ?", new Object[]{richiedente});
		
		for(Map<String, Object> row : rows) {
			Prenotazione prenotazione = new Prenotazione();
			prenotazione.setIdCopia((String)row.get("idCopia"));
			prenotazione.setRichiedente((String)row.get("richiedente"));
			prenotazione.setProprietario((String)row.get("proprietario"));
			prenotazione.setStato((String)row.get("stato"));
			prenotazioni.add(prenotazione);
		}
		
		return prenotazioni;
	}
	
	@Override
	public  List<Prenotazione> findByProprietario(String proprietario) {
		List<Prenotazione> prenotazioni = new ArrayList<Prenotazione>();
		List<Map<String,Object>> rows = jdbcTemplate.queryForList("select * from " + table + " where proprietario = ?", new Object[]{proprietario});
		
		for(Map<String, Object> row : rows) {
			Prenotazione prenotazione = new Prenotazione();
			prenotazione.setIdCopia((String)row.get("idCopia"));
			prenotazione.setRichiedente((String)row.get("richiedente"));
			prenotazione.setProprietario((String)row.get("proprietario"));
			prenotazione.setStato((String)row.get("stato"));
			prenotazioni.add(prenotazione);
		}
		
		return prenotazioni;
	}
	
}

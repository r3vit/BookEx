package com.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;
	
	
	//user store: riferimento al posto dove vengono salvati gli utenti
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.jdbcAuthentication().dataSource(dataSource)     //lo user store è il datasource
			.usersByUsernameQuery("select username,password, enabled from users where username=?")
			.authoritiesByUsernameQuery("select username, role from user_roles where username=?");
	}	
	
	//configurazione per richieste http (chi è autorizzato e chi no)
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
		    .antMatchers("/").authenticated()
		    .antMatchers("/home").authenticated()
			.antMatchers("/myprofile").authenticated()
			.antMatchers("/addbook").authenticated()
			.antMatchers("/addbookresults").authenticated()
			.antMatchers("/addbookfromscratch").authenticated()
			.antMatchers("/addbookenrich").authenticated()
			.antMatchers("/search").authenticated()
			.antMatchers("/api/v1/search").authenticated()
			.antMatchers("/searchresults").authenticated()
			.antMatchers("/mybooks").authenticated()
			.antMatchers("/mytransactions").authenticated()
			.antMatchers("/reserve/*").authenticated()
			.antMatchers("/aceptreservation/*").authenticated()
			.antMatchers("/cancelreservation/*").authenticated()
			.antMatchers("/sendbook/*").authenticated()
			.antMatchers("/bookrecieved/*").authenticated()
			.antMatchers("/booknotrecieved/*").authenticated()
			.antMatchers("/ok/*").authenticated()
			.antMatchers("/submitbook/*").authenticated()
			.antMatchers("/delete/*").authenticated()
			.and()
				.formLogin().loginPage("/login").failureUrl("/login?error")
					.usernameParameter("username").passwordParameter("password")
			.and()
				.logout().logoutSuccessUrl("/login?logout")
			.and()
				.exceptionHandling().accessDeniedPage("/403")
			.and()
				.csrf();
		
	}
}
package com.model;

import java.util.UUID;

import org.json.simple.JSONObject;

public class DomainLibro {
	private String id;
	private String isbn;
	private String proprietario;
	private String annoAcquisto;
	private String statoCopertina;
	private String isSottolienato;
	private String pagineIngiallite;
	private String pagineIlleggibili;
	private String commenti;
	private String stato;
	private int valorePunti;
	private String titolo;
	private String autori;
	private String editore;
	private String annoPubblicazione;
	private String generi;
	private String lingua;
	private int numPagine;
	private String descrizione;
	private String img;
	
	public DomainLibro() {
		this.id = UUID.randomUUID().toString();
		this.isbn = "";
		this.proprietario = "";
		this.annoAcquisto = "";
		this.statoCopertina = "";
		this.isSottolienato = "";
		this.pagineIngiallite = ""; 
		this.pagineIlleggibili = "";
		this.commenti = "";
		this.stato = "disponibile";
		this.valorePunti = 0;
		this.isbn = "";
		this.titolo = "";
		this.autori = "";
		this.editore = "";
		this.annoPubblicazione = "";
		this.generi = "";
		this.lingua = "";
		this.numPagine = 0;
		this.descrizione = "";
		this.img = "img/not-available.jpg";
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getProprietario() {
		return proprietario;
	}
	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}
	public String getAnnoAcquisto() {
		return annoAcquisto;
	}
	public void setAnnoAcquisto(String annoAcquisto) {
		this.annoAcquisto = annoAcquisto;
	}
	public String getStatoCopertina() {
		return statoCopertina;
	}
	public void setStatoCopertina(String statoCopertina) {
		this.statoCopertina = statoCopertina;
	}
	public String getIsSottolienato() {
		return isSottolienato;
	}
	public void setIsSottolienato(String isSottolienato) {
		this.isSottolienato = isSottolienato;
	}
	public String getPagineIngiallite() {
		return pagineIngiallite;
	}
	public void setPagineIngiallite(String pagineIngiallite) {
		this.pagineIngiallite = pagineIngiallite;
	}
	public String getPagineIlleggibili() {
		return pagineIlleggibili;
	}
	public void setPagineIlleggibili(String pagineIlleggibili) {
		this.pagineIlleggibili = pagineIlleggibili;
	}
	public String getCommenti() {
		return commenti;
	}
	public void setCommenti(String commenti) {
		this.commenti = commenti;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	public int getValorePunti() {
		return valorePunti;
	}
	public void setValorePunti(int valorePunti) {
		this.valorePunti = valorePunti;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getAutori() {
		return autori;
	}
	public void setAutori(String autori) {
		this.autori = autori;
	}
	public String getEditore() {
		return editore;
	}
	public void setEditore(String editore) {
		this.editore = editore;
	}
	public String getAnnoPubblicazione() {
		return annoPubblicazione;
	}
	public void setAnnoPubblicazione(String annoPubblicazione) {
		this.annoPubblicazione = annoPubblicazione;
	}
	public String getGeneri() {
		return generi;
	}
	public void setGeneri(String generi) {
		this.generi = generi;
	}
	public String getLingua() {
		return lingua;
	}
	public void setLingua(String lingua) {
		this.lingua = lingua;
	}
	public int getNumPagine() {
		return numPagine;
	}
	public void setNumPagine(int numPagine) {
		this.numPagine = numPagine;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	
	
	public void setFromLibroAndCopia(Libro libro, Copia copia) {
		setId(copia.getId());
		setIsbn(copia.getIsbn());
		setProprietario(copia.getProprietario());
		setAnnoAcquisto(copia.getAnnoAcquisto());
		setStatoCopertina(copia.getStatoCopertina());
		setIsSottolienato(copia.getIsSottolienato());
		setPagineIlleggibili(copia.getPagineIlleggibili());
		setPagineIngiallite(copia.getPagineIngiallite());
		setCommenti(copia.getCommenti());
		setStato(copia.getStato());
		setValorePunti(copia.getValorePunti());
		setTitolo(libro.getTitolo());
		setAutori(libro.getAutori());
		setEditore(libro.getEditore());
		setAnnoPubblicazione(libro.getAnnoPubblicazione());
		setGeneri(libro.getGeneri());
		setLingua(libro.getLingua());
		setNumPagine(libro.getNumPagine());
		setDescrizione(libro.getDescrizione());
		setImg(libro.getImg());
	}
	
	
	public void setFromJSONObject(JSONObject doc) {
		setTitolo(doc.get("titolo").toString());
		setAutori(doc.get("autori").toString());
		setEditore(doc.get("editore").toString());
		setAnnoPubblicazione(doc.get("annoPubblicazione").toString());
		setGeneri(doc.get("generi").toString());
		setLingua(doc.get("lingua").toString());
		setNumPagine(Integer.parseInt(doc.get("numPagine").toString()));
		setDescrizione(doc.get("descrizione").toString());
		setImg(doc.get("img").toString());
		
		setId(doc.get("id").toString());
		setIsbn(doc.get("isbn").toString());
		setProprietario(doc.get("proprietario").toString());
		setAnnoAcquisto(doc.get("annoAcquisto").toString());
		setStatoCopertina(doc.get("statoCopertina").toString());
		setIsSottolienato(doc.get("isSottolienato").toString());
		setPagineIngiallite(doc.get("pagineIngiallite").toString());
		setPagineIlleggibili(doc.get("pagineIlleggibili").toString());
		setCommenti(doc.get("commenti").toString());
		setStato(doc.get("stato").toString());
		setValorePunti(Integer.parseInt(doc.get("valorePunti").toString()));
	}
}

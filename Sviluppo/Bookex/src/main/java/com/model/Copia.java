package com.model;

import java.util.UUID;

public class Copia {
	private String id;
	private String isbn;
	private String proprietario;
	private String annoAcquisto;
	private String statoCopertina;
	private String isSottolienato;
	private String pagineIngiallite;
	private String pagineIlleggibili;
	private String commenti;
	private String stato;
	private int valorePunti;


	public Copia() {
		this.id = UUID.randomUUID().toString();
		this.isbn = "";
		this.proprietario = "";
		this.annoAcquisto = "";
		this.statoCopertina = "";
		this.isSottolienato = "";
		this.pagineIngiallite = ""; 
		this.pagineIlleggibili = "";
		this.commenti = "";
		this.stato = "disponibile";
		this.valorePunti = 0;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getProprietario() {
		return proprietario;
	}
	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}
	public String getAnnoAcquisto() {
		return annoAcquisto;
	}
	public void setAnnoAcquisto(String annoAcquisto) {
		this.annoAcquisto = annoAcquisto;
	}
	public String getStatoCopertina() {
		return statoCopertina;
	}
	public void setStatoCopertina(String statoCopertina) {
		this.statoCopertina = statoCopertina;
	}
	public String getIsSottolienato() {
		return isSottolienato;
	}
	public void setIsSottolienato(String isSottolienato) {
		this.isSottolienato = isSottolienato;
	}
	public String getPagineIngiallite() {
		return pagineIngiallite;
	}
	public void setPagineIngiallite(String pagineIngiallite) {
		this.pagineIngiallite = pagineIngiallite;
	}
	public String getPagineIlleggibili() {
		return pagineIlleggibili;
	}
	public void setPagineIlleggibili(String pagineIlleggibili) {
		this.pagineIlleggibili = pagineIlleggibili;
	}
	public String getCommenti() {
		return commenti;
	}
	public void setCommenti(String commenti) {
		this.commenti = commenti;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	public int getValorePunti() {
		return valorePunti;
	}

	public void setValorePunti(int valorePunti) {
		this.valorePunti = valorePunti;
	}
	
	
	public void setFromDomainLibro(DomainLibro domainLibro) {
		setIsbn(domainLibro.getIsbn());
	    setProprietario(domainLibro.getProprietario());
	    setAnnoAcquisto(domainLibro.getAnnoAcquisto());
	    setStatoCopertina(domainLibro.getStatoCopertina());
	    setIsSottolienato(domainLibro.getIsSottolienato());
	    setPagineIngiallite(domainLibro.getPagineIngiallite());
	    setPagineIlleggibili(domainLibro.getPagineIlleggibili());
	    setCommenti(domainLibro.getCommenti());
	    setStato("disponibile");
	    setValorePunti(domainLibro.getValorePunti());
	}
}
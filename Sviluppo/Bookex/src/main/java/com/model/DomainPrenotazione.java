package com.model;



public class DomainPrenotazione {
	
	private Prenotazione prenotazione;
	private DomainLibro domainLibro;

	public Prenotazione getPrenotazione() {
		return prenotazione;
	}

	public void setPrenotazione(Prenotazione prenotazione) {
		this.prenotazione = prenotazione;
	}
	public DomainLibro getDomainLibro() {
		return domainLibro;
	}

	public void setDomainLibro(DomainLibro domainLibro) {
		this.domainLibro = domainLibro;
	}
	
	
	
}

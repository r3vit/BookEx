package com.model;

public class Prenotazione {
	private String idCopia;
	private String richiedente;
	private String proprietario;
	private String stato;
	
	
	public String getIdCopia() {
		return idCopia;
	}
	public void setIdCopia(String idCopia) {
		this.idCopia = idCopia;
	}
	public String getRichiedente() {
		return richiedente;
	}
	public void setRichiedente(String richiedente) {
		this.richiedente = richiedente;
	}
	public String getProprietario() {
		return proprietario;
	}
	public void setProprietario(String possessore) {
		this.proprietario = possessore;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	
}

package com.model;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

@SolrDocument(solrCoreName = "libriSolr")
public class Libro {
	
	@Id
	@Field
	private String isbn;
	@Field
	private String titolo;
	private String autori;
	private String editore;
	private String annoPubblicazione;
	private String generi;
	private String lingua;
	private int numPagine;
	private String descrizione;
	private String img;
	
	
	public Libro () {
		this.isbn = "";
		this.titolo = "";
		this.autori = "";
		this.editore = "";
		this.annoPubblicazione = "";
		this.generi = "";
		this.lingua = "";
		this.numPagine = 0;
		this.descrizione = "";
		this.img = "img/not-available.jpg";
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getAutori() {
		return autori;
	}
	public void setAutori(String autori) {
		this.autori = autori;
	}
	public String getEditore() {
		return editore;
	}
	public void setEditore(String editore) {
		this.editore = editore;
	}
	public String getAnnoPubblicazione() {
		return annoPubblicazione;
	}
	public void setAnnoPubblicazione(String annoPubblicazione) {
		this.annoPubblicazione = annoPubblicazione;
	}
	public String getGeneri() {
		return generi;
	}
	public void setGeneri(String generi) {
		this.generi = generi;
	}
	public String getLingua() {
		return lingua;
	}
	public void setLingua(String lingua) {
		this.lingua = lingua;
	}
	public int getNumPagine() {
		return numPagine;
	}
	public void setNumPagine(int numPagine) {
		this.numPagine = numPagine;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}

	
	public void setFromDomainLibro(DomainLibro domainLibro) {
	    setIsbn(domainLibro.getIsbn());
	    setTitolo(domainLibro.getTitolo());
	    setAutori(domainLibro.getAutori());
	    setEditore(domainLibro.getEditore());
	    setAnnoPubblicazione(domainLibro.getAnnoAcquisto());
	    setGeneri(domainLibro.getGeneri());
	    setLingua(domainLibro.getLingua());
	    setNumPagine(domainLibro.getNumPagine());
	    setDescrizione(domainLibro.getDescrizione());
	    setImg(domainLibro.getImg());		
	}
}

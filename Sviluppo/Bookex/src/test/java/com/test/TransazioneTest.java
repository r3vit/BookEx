package com.test;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.config.BookexConfig;
import com.model.Copia;
import com.model.Libro;
import com.model.Prenotazione;
import com.model.Utente;
import com.repository.LibroRepo;
import com.service.CopiaService;
import com.service.LibroService;
import com.service.PrenotazioneService;
import com.service.UtenteService;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookexConfig.class)
@AutoConfigureMockMvc
public class TransazioneTest {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private CopiaService copiaService;
	@Autowired
	private LibroRepo libroRepo;
	@Autowired
	private LibroService libroService;
	@Autowired
	private PrenotazioneService prenotazioneService;
	@Autowired
	private UtenteService utenteService;

	@Before
	@After
	public void before() {
		libroRepo.deleteAll();
		libroService.deleteAll();
		copiaService.deleteAll();
		prenotazioneService.deleteAll();
		Utente rAlvaPrincipe = utenteService.findByUsername("rAlvaPrincipe");
		rAlvaPrincipe.setPunti(10);
		rAlvaPrincipe.setPuntiImpegnati(0);
		utenteService.update(rAlvaPrincipe);
		Utente mCapobussi = utenteService.findByUsername("mCapobussi");
		mCapobussi.setPunti(10);
		utenteService.update(mCapobussi);
	}

	@Test
	public void test1() throws Exception {
		Copia heyOhLetsGo = insert("mCapobussi", "hey oh! let's go!", 10);

		action("reserve", "rAlvaPrincipe", heyOhLetsGo);
		action("aceptreservation", "mCapobussi", heyOhLetsGo);
		action("sendbook", "mCapobussi", heyOhLetsGo);
		action("bookreceived", "rAlvaPrincipe", heyOhLetsGo);
		action("ok", "mCapobussi", heyOhLetsGo);

		Utente rAlvaPrincipe = utenteService.findByUsername("rAlvaPrincipe");
		Assert.assertTrue(rAlvaPrincipe.getPunti() == 0);
		Assert.assertTrue(rAlvaPrincipe.getPuntiImpegnati() == 0);

		Utente mCapobussi = utenteService.findByUsername("mCapobussi");
		Assert.assertTrue(mCapobussi.getPunti() == 10 + heyOhLetsGo.getValorePunti());
	}

	@Test
	public void test2() throws Exception {
		Copia heyOhLetsGo = insert("mCapobussi", "hey oh! let's go!", 10);
		Utente mCapobussi = utenteService.findByUsername("mCapobussi");
		mCapobussi.setPunti(0);
		utenteService.update(mCapobussi);

		action("reserve", "rAlvaPrincipe", heyOhLetsGo);
		action("aceptreservation", "mCapobussi", heyOhLetsGo);
		action("sendbook", "mCapobussi", heyOhLetsGo);
		action("cancelreservation", "rAlvaPrincipe", heyOhLetsGo);

		Prenotazione prenotazione = prenotazioneService.findByIdCopia(heyOhLetsGo.getId());
		Assert.assertTrue(prenotazione.getStato().equals("spedito"));
	}

	@Test
	public void test3() throws Exception {
		Copia heyOhLetsGo = insert("mCapobussi", "hey oh! let's go!", 7);

		action("reserve", "rAlvaPrincipe", heyOhLetsGo);
		action("aceptreservation", "mCapobussi", heyOhLetsGo);
		action("cancelreservation", "rAlvaPrincipe", heyOhLetsGo);

		Prenotazione prenotazione = prenotazioneService.findByIdCopia(heyOhLetsGo.getId());
		Assert.assertTrue(prenotazione.getStato().equals("prenotazione_accettata"));
	}

	/********************************** SUPPORT *********************************/

	public void action(String azione, String richiedenteAzione, Copia copia) throws Exception {
		mockMvc.perform(post("/" + azione + "/" + copia.getId())
				.with(user(richiedenteAzione))
				.with(csrf()));
	}

	
	public Copia insert(String proprietario, String titolo, int valorePunti) {
		Libro libro = new Libro();
		Copia copia = new Copia();

		libro.setTitolo(titolo);
		copia.setValorePunti(valorePunti);
		copia.setProprietario(proprietario);

		if (titolo.equals("hey oh! let's go!")) {
			libro.setIsbn("1119999999");
			copia.setIsbn("1119999999");
		}

		libroService.insert(libro);
		libroRepo.save(libro);
		copiaService.insert(copia);
		return copia;
	}
}
package com.test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.config.BookexConfig;
import com.model.Copia;
import com.model.Libro;
import com.repository.LibroRepo;
import com.service.CopiaService;
import com.service.LibroService;

import org.junit.After;
import org.junit.Before;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookexConfig.class)
@AutoConfigureMockMvc

public class RicercaIsbnTest {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private CopiaService copiaService;
	@Autowired
	private LibroRepo libroRepo;
	@Autowired
	private LibroService libroService;

	@Before
	@After
	public void before() {
		libroRepo.deleteAll();
		libroService.deleteAll();
		copiaService.deleteAll();
	}

	@Test
	@WithMockUser(username = "rAlvaPrincipe")
	public void test1() throws Exception {
		insert("mCapobussi", "Il Piccolo Principe", true);
		mockMvc.perform(get("/api/v1/search").param("query", "978-15-07-17064-9"))
				.andExpect(content().string(containsString("Il Piccolo Principe")));
	}

	@Test
	@WithMockUser(username = "rAlvaPrincipe")
	public void test2() throws Exception {
		insert("rAlvaPrincipe", "Il Piccolo Principe", false);
		insert("mCapobussi", "Il Piccolo Principe", false);
		insert("mValzelli", "Il Piccolo Principe", false);

		mockMvc.perform(get("/api/v1/search").param("query", "1507170645"))
				.andExpect(content().string(StringMatcher.containsStringNTimes("Il Piccolo Principe", 2)));
	}

	@Test
	@WithMockUser(username = "rAlvaPrincipe")
	public void test3() throws Exception {
		insert("mCapobussi", "Il Piccolo Principe", true);

		mockMvc.perform(get("/api/v1/search").param("query", "978-15-07-17064-2"))
				.andExpect(content().string(containsString("{ \"copie\": [ ]}")));
	}

	/********************************** SUPPORT *********************************/

	public Copia insert(String proprietario, String titolo, boolean isIsbn13) {
		Libro libro = new Libro();
		Copia copia = new Copia();

		libro.setTitolo(titolo);
		copia.setProprietario(proprietario);

		if (titolo.equals("Il Piccolo Principe")) {
			if (isIsbn13) {
				libro.setIsbn("9781507170649");
				copia.setIsbn("9781507170649");
			} else {
				libro.setIsbn("1507170645");
				copia.setIsbn("1507170645");
			}
		}

		libroService.insert(libro);
		libroRepo.save(libro);
		copiaService.insert(copia);
		return copia;
	}
}

package com.test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.config.BookexConfig;
import com.model.Copia;
import com.model.Libro;
import com.repository.LibroRepo;
import com.service.CopiaService;
import com.service.LibroService;

import org.junit.After;
import org.junit.Before;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookexConfig.class)
@AutoConfigureMockMvc
public class RicercaKeywordTest {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private CopiaService copiaService;
	@Autowired
	private LibroRepo libroRepo;
	@Autowired
	private LibroService libroService;

	@Before
	@After
	public void before() {
		libroRepo.deleteAll();
		libroService.deleteAll();
		copiaService.deleteAll();
	}

	@Test
	@WithMockUser(username = "rAlvaPrincipe")
	public void test1() throws Exception {
		insert("mCapobussi", "hey oh! let's go!");

		mockMvc.perform(get("/api/v1/search").param("query", ""))
				.andExpect(content().string(containsString("{ \"copie\": [ ]}")));
	}

	@Test
	@WithMockUser(username = "rAlvaPrincipe")
	public void test2() throws Exception {
		insert("mCapobussi", "hey oh! let's go!");
		insert("mCapobussi", "i wanna rock");
		insert("rAlvaPrincipe", "you! let's go!");

		mockMvc.perform(get("/api/v1/search").param("query", "go!"))
				.andExpect(content().string(containsString("hey oh! let's go!")))
				.andExpect(content().string(not(containsString("you! let's go!"))))
				.andExpect(content().string(not(containsString("i wanna rock"))));
	}

	@Test
	@WithMockUser(username = "rAlvaPrincipe")
	public void test3() throws Exception {
		insert("mCapobussi", "let's go!");
		insert("mValzelli", "you! let's go!");
		insert("mValzelli", "hey oh! let's go!");
		insert("mValzelli", "i wanna rock");

		mockMvc.perform(get("/api/v1/search").param("query", "let's go!"))
				.andExpect(content().string(containsString("let's go!")))
				.andExpect(content().string(containsString("you! let's go!")))
				.andExpect(content().string(containsString("hey oh! let's go!")))
				.andExpect(content().string(not(containsString("i wanna rock"))));
	}

	/********************************** SUPPORT *********************************/

	public Copia insert(String proprietario, String titolo) {
		Libro libro = new Libro();
		Copia copia = new Copia();

		libro.setTitolo(titolo);
		copia.setProprietario(proprietario);

		if (titolo.equals("hey oh! let's go!")) {
			libro.setIsbn("1119999999");
			copia.setIsbn("1119999999");
		} else if (titolo.equals("you! let's go!")) {
			libro.setIsbn("2229999999");
			copia.setIsbn("2229999999");
		} else if (titolo.equals("let's go!")) {
			libro.setIsbn("3339999999");
			copia.setIsbn("3339999999");
		} else if (titolo.equals("i wanna rock")) {
			libro.setIsbn("4449999999");
			copia.setIsbn("4449999999");
		}

		libroService.insert(libro);
		libroRepo.save(libro);
		copiaService.insert(copia);
		return copia;
	}
}

#!/bin/bash

function start(){
	./solr-7.0.0/bin/solr start -force
	cd Bookex
	mvn spring-boot:run
}

function stop(){
	./solr-7.0.0/bin/solr stop
}


function test(){
	./solr-7.0.0/bin/solr start -force
	cd Bookex
	mvn test
}


function init(){
	mysql --user="root" --password="password" --execute="DROP DATABASE bookex;"
	mysql --user="root" --password="password" --execute="source scripts/schemas.sql"
}


function config(){
	username=$1
	password=$2

	sed -i  "s/\-\-user\=\"[a-z,A-Z,0-9]*\"/\-\-user\=\"$username\"/g" bookex.sh
	sed -i  "s/\-\-password\=\".*\" --/\-\-password\=\"$password\" --/g" bookex.sh

	cd Bookex/src/main/resources

	sed -i  "s/spring.datasource.username.*/spring.datasource.username\=$username/g" application.properties
	sed -i  "s/spring.datasource.password.*/spring.datasource.password\=$password/g" application.properties
}


case "$1" in
        start)
			start 
			;;
        stop)
            stop
            ;;
        init)
            init
            ;;
        config)
            config $2 $3
            ;; 
        test)
            test
            ;;
        *)
        	echo "Usage: bookex init | config | start | stop "
			;;
esac

CREATE DATABASE bookex;

USE bookex;

CREATE  TABLE users (
  username VARCHAR(45) NOT NULL ,
  password VARCHAR(45) NOT NULL ,
  nome VARCHAR(45) NOT NULL ,
  cognome VARCHAR(45) NOT NULL ,
  sesso VARCHAR(1) NOT NULL ,
  indirizzo VARCHAR(60) NOT NULL ,
  citta VARCHAR(45) NOT NULL ,
  provincia VARCHAR(45) NOT NULL ,
  cap VARCHAR(5) NOT NULL ,
  telefono VARCHAR(12) NOT NULL ,
  email VARCHAR(45) NOT NULL ,
  punti INT(5) NOT NULL ,
  puntiImpegnati INT(5) NOT NULL ,
  enabled TINYINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (username));


CREATE TABLE user_roles (
  user_role_id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(45) NOT NULL,
  role varchar(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  UNIQUE KEY uni_username_role (role,username),
  KEY fk_username_idx (username),
  CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES users (username));


CREATE  TABLE libri (
  isbn VARCHAR(45) NOT NULL ,
  titolo VARCHAR(60) NOT NULL ,
  autori VARCHAR(60),
  editore VARCHAR(60) NOT NULL ,
  annoPubblicazione VARCHAR(10),
  generi VARCHAR(60) ,
  lingua VARCHAR(45) NOT NULL ,
  numPagine VARCHAR(10) ,
  descrizione VARCHAR(3000) ,
  img VARCHAR(500) ,
  PRIMARY KEY (isbn));



CREATE  TABLE copie (
  id VARCHAR(45) NOT NULL ,
  isbn VARCHAR(45) NOT NULL ,
  proprietario VARCHAR(45) NOT NULL ,
  annoAcquisto VARCHAR(10) ,
  statoCopertina VARCHAR(45) NOT NULL ,
  isSottolineato VARCHAR(45) NOT NULL ,
  pagineIngiallite VARCHAR(45) NOT NULL ,
  pagineIlleggibili VARCHAR(45) NOT NULL ,
  commenti VARCHAR(1000) ,
  stato VARCHAR(45) NOT NULL ,
  valorePunti INT(10) NOT NULL ,
  PRIMARY KEY (id));


CREATE  TABLE prenotazioni (
  idCopia VARCHAR(45) NOT NULL ,
  richiedente VARCHAR(45) NOT NULL ,
  proprietario VARCHAR(45) NOT NULL ,
  stato VARCHAR(45) NOT NULL ,
  PRIMARY KEY (idCopia));



INSERT INTO users(username,password,nome,cognome,sesso,indirizzo,citta,provincia,cap,telefono,email,punti, puntiImpegnati, enabled)
VALUES ('rAlvaPrincipe','lab_progettazione1', 'Renzo Arturo', 'Alva Principe', 'M', 'via Garibaldi 10', 'Muggiò', 'MB', '20835', '3454557788', 'r.alvaprincipe@labprog.it', 10, 0, true);
INSERT INTO users(username,password,nome,cognome,sesso,indirizzo,citta,provincia,cap,telefono,email,punti, puntiImpegnati,  enabled)
VALUES ('mValzelli', 'lab_progettazione1', 'Marco', 'Valzelli', 'M', 'via Roma 32', 'Desio', 'MB', '20835', '3454777454', 'm.valzelli@labprog.it', 10, 0, true);
INSERT INTO users(username,password,nome,cognome,sesso,indirizzo,citta,provincia,cap,telefono,email,punti, puntiImpegnati, enabled)
VALUES ('mCapobussi','lab_progettazione1', 'Marco', 'Capobussi', 'M', 'via Carlo Porta 3', 'Lissone', 'MB', '20851', '3454112237', 'm.capobussi@labprog.it', 10, 0, true);

INSERT INTO user_roles (username, role)
VALUES ('rAlvaPrincipe', 'ROLE_USER');
INSERT INTO user_roles (username, role)
VALUES ('rAlvaPrincipe', 'ROLE_ADMIN');
INSERT INTO user_roles (username, role)
VALUES ('mValzelli', 'ROLE_USER');
INSERT INTO user_roles (username, role)
VALUES ('mCapobussi', 'ROLE_USER');

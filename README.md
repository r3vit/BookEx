# 📚 BookEx

BookEx è una piattaforma online pensata per supportare uno scambio peer to peer di libri, testi scolastici, riviste e fumetti.


## Informazioni generali

Il progetto **BookEx** si riferisce al corso di Laboratorio di Progettazione erogato dall'Università degli Studi di Milano Bicocca nell'anno accademico 2017/2018.

### Componenti

Marco Capobussi

Marco Valzelli

Renzo Arturo Alva Principe


### Installazione

In questa sezione verranno descritte le fasi per ottenere una installazione corretta e completa del progetto im ambiente Linux.
Da eseguire in ordine:

**Dipendeze SW:**

- mysql server
- maven
- java 8


`$ cd Sviluppo`

**Configurazione DB:**

`$ ./bookex.sh config mysql_username mysql_password` # setting credenziali mysql 

**Inizializzazione:**

`$ ./bookex.sh init`

**Testing:**

`$ ./bookex.sh test`

**Start:**

`$ ./bookex.sh start` 

**Stop:**

`$ ./bookex.sh stop` 

visita http://localhost:8080/

Sviluppato e testato su Ubuntu 18.04 LTS



